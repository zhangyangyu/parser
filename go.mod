module modernc.org/parser

go 1.15

require (
	modernc.org/golex v1.0.1
	modernc.org/mathutil v1.4.1 // indirect
	modernc.org/scanner v1.0.1
	modernc.org/strutil v1.1.0
)
