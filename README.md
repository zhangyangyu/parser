parser
======

Collection of Go parsers

- [N-Quads](http://www.w3.org/TR/n-quads/): [http://godoc.org/modernc.org/parser/nquads](http://godoc.org/modernc.org/parser/nquads)
- [yacc](http://pubs.opengroup.org/onlinepubs/009695399/utilities/yacc.html): [http://godoc.org/modernc.org/parser/yacc](http://godoc.org/modernc.org/parser/yacc)
